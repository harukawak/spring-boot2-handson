package jp.co.elpiscorp.boothandson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoothandsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoothandsonApplication.class, args);
	}

}
